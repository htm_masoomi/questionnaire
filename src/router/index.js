import Vue from "vue";
import VueRouter from "vue-router";
// import Signup from "../components/logpages/Signup.vue";
// import Login from "../components/logpages/Login.vue";
// import Finish from "../components/questionPages/Finish.vue";
// import QuestionMain from "../components/questionPages/QuestionMain.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/questionnaire-list",
    name: "questionnaire",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/QuestionnaireList.vue"),
  },
  {
    path: "/",
    component: () =>
      import( "../views/AppAuth.vue"),
    children: [
      {
        path: "",
        component : () =>  import(/* webpackChunkName: "auth" */ "@/components/auth/Signin.vue"),
      },
      {
        path: "signup",
        component : () =>  import(/* webpackChunkName: "auth" */ "@/components/auth/Signup"),
      },      
    ],
  },

  // {
  //   path: "/",
  //   name: "signin",
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/Signin.vue"),
  // },

  // {
  //   path: "/signup",
  //   name: "signup",
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/Signup.vue"),
  // },



  {
    path: "/question/:QID",
    name: "question",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AppQuestion.vue"),
  },

  {
    path: "/finish",
    name: "finish",
    component: () =>
      import(/* webpackChunkName: "about" */ "../components/question/Finish.vue"),
  },

  // {
  //   path: "/questions/",
  //   name: "questions",
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/Questions.vue"),
  //   children: [
  //     {
  //       path: "finish",
  //       component: Finish,
  //     },
  //     {
  //       path: ":QID",
  //       component: QuestionMain,
  //     },
  //   ],
  // },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
